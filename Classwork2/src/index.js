// Якщо змінна a дорівнює 10, то виведіть 'Вірно', інакше виведіть 'Неправильно'

let num = parseInt (prompt('Яке число?'));
let res = num === 10 ? 'Вірно!' : 'Неправильно!';
alert (res);


/* У змінній min лежить число від 0 до 59. Визначте, в яку чверть години потрапляє це число
 (У першу, другу, третю або четверту).*/

let numInput = parseInt(prompt('В якій чверті?'));

if (numInput >= 0 && numInput < 15) {
  alert('Перва чверть');
}else if (numInput < 30) {
  alert ('Друга чверть');
}else if (numInput < 45) {
  alert ('Третя чверть');
}else if (numInput < 59) {
  alert ('Четверта чверть');
}else {
  alert ('Невірні дані. Введіть число в проміжку від 0 до 59')
}


/* Змінна num може приймати 4 значення: 1, 2, 3 або 4. Якщо вона має значення '1', то змінну result запишемо 'зима', 
 якщо має значення '2' – 'весна' тощо. Розв'яжіть завдання через switch-case*/

let season = prompt ('Введіть номер пори року');

switch (season) {
  case '1': {
    alert('Winter');
    break;
  }
  case '2': {
    alert('Spring');
    break;
  }
  case '3': {
    alert('Summer');
    break;
  }
  case '4': {
    alert('Autumn');
    break;
  }
  default: {
    alert ('Введіть правильні числа(1, 2, 3, 4)')
  }
}


/*  Використовуючи умовні конструкції перевірте вік користувача, якщо користувачеві буде від 18 до 35 років переведіть 
його на сайт google.com  через 2 секунди, якщо вік користувача буде від 35 до 60 років, переведіть його 
на сайт https://www.uz.gov.ua/,  якщо користувачеві буде до 18 років покажіть йому першу серію лунтика з ютубу. 
Виконайте це завдання за допомогою if, switch, тернарний оператор */

let age = prompt('Вкажіть ваш вік');
if (age >= 18 && age < 35) {
  document.write ('<meta http-equiv="refresh" content="2, url=https://www.google.com">');
}else if (age >= 35 && age <=60) {
  document.write ('<meta http-equiv="refresh" content="2, url=https://www.uz.gov.ua">');
}else if (age <= 18 && age >= 0) {
  document.write ('<meta http-equiv="refresh" content="2, url=https://www.youtube.com/watch?v=3Hz3kHRRISU">');
}else {
  document.write ('Не можливо');
};
