/* Створити об'єкт криптогаманець. В гаманці повинно зберігатися ім'я власника, кілька валют Bitcoin, Ethereum, Stellar
та в кожній валюті додатково є ім'я валюти, логотип, кілька монет та курс на сьогодні. Також в об'єкті гаманець є метод, виклик 
якого він приймає ім'я валюти та виводить на сторінку інформацію. "Добрий день, ...! На вашому балансі (Назва валюти та логотип)
залишилось N монет, якщо ви сьогодні продасте їх, то отримаєте ... грн."
Виведення насторінку повинно бути красиво зроблене з використанням css nf html.*/
const wallet = {
    name: "Vova",
    btc: {
      name: "Bitcoine",
      logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png'>",
      rate: "30245.99",
      coin: "54",
    },
    etn: {
      name: "Ethereum",
      logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png'>",
      rate: "1801.98",
      coin: "27",
    },
    xlm: {
      name: "Stellar",
      logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png'>",
      rate: "0.1402",
      coin: "37",
    },
    show: function (nameCoine) {
      document.getElementById("wallet").innerHTML = `Добрий день, ${
        wallet.name
      }! На вашому балансі ${wallet[nameCoine].name}${
        wallet[nameCoine].logo
      } залишилось ${
        wallet[nameCoine].coin
      } монет, якщо ви сьогодні продасте їх, то отримаєте ${(wallet[nameCoine].rate *wallet[nameCoine].coin*31
      ).toFixed(2)}грн.`;
    },
  };
  
  wallet.show(prompt("Веддіть назви монет:", "btc,etn,xlm"));