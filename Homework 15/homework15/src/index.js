import React from "react";
import ReactDOM from "react-dom";

 

const App = function () {
    return (
        <div>
        <Months></Months>
        <Days></Days>
        <Zodiacs></Zodiacs>
        </div>
    )
};

const month =["January", "February", "March", "June", "July", "August", "September", "October", "November", "December"],
days = ["Monday", "Tuestady", "Wenesday", "Thursday", "Friday", "Saturday", "Sunday"],
zodiac = ["Capricorn", "Aquarius", "Pisces", "Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagitarius"];

const Months = function () {
    const allmonth = month.map(el => <li>{el}</li>);
    return (
        <div>
            <h2>Months</h2>
            <ul>
                {allmonth}
            </ul>
        </div>
    )
};

const Days = function () {
    const alldays = days.map(el => <li>{el}</li>);
    return (
        <div>
            <h2>Days</h2>
            <ul>
                {alldays}
            </ul>
        </div>
    )
};

const Zodiacs = function () {
    const allzodiacs = zodiac.map(el => <li>{el}</li>);
    return (
        <div>
            <h2>Zodiacs</h2>
            <ul>
                {allzodiacs}
            </ul>
        </div>
    )
}

ReactDOM.render(<App></App>, document.querySelector("#root"));