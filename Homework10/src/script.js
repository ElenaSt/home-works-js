const btn = document.querySelector(".keys"),
display = document.getElementById("display"),
displayOut = document.querySelector(".display");
let result = "",
memory = "",
cleanMemory = false,
value1 = "",
value2 = "",
sign = "";

const pressNumber = (value) => {
    if (value2 == "" && sign == "") {
        value1 += value;
        display.value = value1;
    } else if (value1 !== "" && sign !== "") {
        value2 += value;
        display.value = value2;
    } else if (value1 !== "" && sign !== "" && value2 == "") {
        value2 += value1;
        
    } 
}
const clear = () => {
    value1 = "";
    value2 = "";
    sign = "";
}

const resultAll = () => {
    if (value1 == "") return;
    switch (sign) {
        case "+": {
            if (value2 == "") {value2=value1}
            result = Number(value1) + Number(value2);
            display.value = result;
            
            break;
        }
        case "-": {
            result = Number(value1) - Number(value2);
            display.value = result;
            
            break;
        }
        case "/": {
            result = value2 === "0" ? "Помилка" : Number(value1) / Number(value2);
            display.value = result;
            
            break;
        }
        case "*": {
            result = Number(value1) * Number(value2);
            display.value = result;
            
            break;
        }
    }
}

btn.addEventListener("click", (event) => {
    event.preventDefault();

    const element = event.target;
    if (element.getAttribute("type") !== "button") return;
    const elVal = element.value;

    if (elVal.match(/[\d.]/)) {
        pressNumber(elVal);
    } else if (elVal.match(/[+]/) && elVal !== "m-" && elVal !== "m+") {
        display.value = "+";
        sign = elVal;
    } else if (elVal.match(/[-]/) && elVal !== "m-" && elVal !== "m+") {
            display.value = "-";
            sign = elVal;
        } else if (elVal.match(/[*]/) && elVal !== "m-" && elVal !== "m+") {
                display.value = "*";
                sign = elVal;
            } else if (elVal.match(/[/]/) && elVal !== "m-" && elVal !== "m+") {
                    display.value = "/";
                    sign = elVal;
    } else {
        switch (elVal) {
            case "=": {
                display.value = "";
                resultAll();
                clear();
                break;
            }
            case "C": {
                display.value = "0";
                clear()
                break;
            }
            case "m-": {
                if (!displayOut.classList.contains("memory")) displayOut.classList.add("memory");
                memory = memory === "" ? display.value : Number(memory) - Number(display.value);
                break;
            }
            case "m+": {
                if (!displayOut.classList.contains("memory")) displayOut.classList.add("memory");
                memory = memory === "" ? display.value : Number(memory) + Number(display.value);
                break;
            }
            case "mrc": {
                if (!cleanMemory) {
                    display.value = memory 
                    cleanMemory = true;
                    return;
                }
                memory = "";
                cleanMemory = false;
                displayOut.classList.remove("memory");
                break;
            }
        }
    }
})
