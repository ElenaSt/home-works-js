/*1.Створи клас, який буде створювати користувачів з ім'ям та прізвищем. 
Додати до класу метод для виведення імені та прізвища*/
window.addEventListener("DOMContentLoaded", () => {
const inputName = document.createElement("input"),
    inputLastname = document.createElement("input"),
    spanout = document.createElement("span"),
    spanoutl = document.createElement("span");

inputName.className = "name";
inputName.placeholder = "Name";
inputLastname.className = "last-name";
inputLastname.placeholder = "last name";
spanout.className = "out-name";

let user = {
    name: "",
    lastName: "",
    inputUser: function () {
        this.name = inputName.value;
        this.lastName = inputLastname.value;
    },
}

document.body.prepend(inputName);
inputName.after(inputLastname);
inputLastname.after(spanout);
spanout.after(spanoutl);
inputName.before("Поля вводу ім'я та фамілії:");

inputName.addEventListener("input", (e) => {
    spanout.innerText = `Ім'я: ${e.target.value}`
});
inputLastname.addEventListener("input", (e) => {
    spanoutl.innerText = `Прізвище: ${e.target.value}`
});
/*2.Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li 
і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний*/
const ul = document.createElement("ul"),
    li1 = document.createElement("li"),
    li2 = document.createElement("li"),
    li3 = document.createElement("li"),
    li4 = document.createElement("li");

spanoutl.after(ul);
ul.prepend(li1);
ul.append(li2);
ul.append(li3);
ul.append(li4);

li3.addEventListener("click", (e) => {
    // target - елемент, котрий ініціював дію.
    e.target.style.backgroundColor = "red";
})
li4.addEventListener("click", (e) => {
    e.target.style.backgroundColor = "blue";
})

/*3.Створи див висотою 400 пікселів і додай на нього подію наведення мишки. 
При наведенні мишки виведіть текст координати, де знаходиться курсор мишки*/

    const div = document.createElement("div");
    div.className = "box";

    div.onmousemove = function (e) {
        this.innerHTML = "Наведення мишки X:" + e.offsetX + " Y:" + e.offsetY;
    }
    document.body.prepend(div);


/*4.Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, 
яка кнопка була натиснута*/

    const btn1 = document.createElement("button"),
        btn2 = document.createElement("button"),
        btn3 = document.createElement("button"),
        btn4 = document.createElement("button"),
        btn = document.createElement("div"),
        output = document.createElement("output");


    document.body.append(btn);
    btn.prepend(btn1);
    btn.append(btn2);
    btn.append(btn3);
    btn.append(btn4);
    btn4.after(output);

    btn.classList.add("btn");
    btn1.innerText = "1";
    btn1.value = "1";

    btn2.innerText = "2";
    btn2.value = "2";

    btn3.innerText = "3";
    btn3.value = "3";

    btn4.innerText = "4";
    btn4.value = "4";

    document.querySelectorAll("button").forEach(e => {
        e.addEventListener("click", () => {
            document.querySelector("output").value =
                `Натиснута ${e.value} кнопка`;
        })
    })


/*5.Cтвори див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці*/
const divMove = document.createElement("div");
divMove.className = "div-move";

document.body.append(divMove);

/*divMove.addEventListener("mouseover", (e) =>{
    divMove.classList.add("animDiv");
});*/
divMove.addEventListener("mouseover", (e) =>{
    e.target.classList.add("animDiv");
    let left = e.clientX;
    let top = e.clientY;
    divMove.style.left = left + "px";
    divMove.style.top = top + "px";
    e.preventDefault();
});


/*6.Створи поле для введення кольору, коли користувач вибере якийсь колір, 
зроби його фоном body*/


    if (window.localStorage.pageColor) {
        document.body.style.backgroundColor = window.localStorage.pageColor;
    }

    document.getElementById("BackroundColor").addEventListener("click", () => {
        const selectedColor = document.querySelector('[type=color]').value
        document.body.style.backgroundColor = selectedColor;
    });
});
/*7.Створи інпут для введення логіну, коли користувач почне вводити 
дані в інпут виводь їх в консоль*/
window.addEventListener("DOMContentLoaded", () => {
    const inputLogin = document.createElement("input");

    inputLogin.placeholder = "Login";
    inputLogin.type = "text";
    document.body.append(inputLogin);

    inputLogin.addEventListener("input", (e) => {
        console.log(`Логін: ${e.target.value}`);
    })

/*8.Створіть поле для введення даних у полі введення даних виведіть 
текст під полем*/
const inputData = document.createElement ("textarea"),
      spanOutput = document.createElement ("span");

inputData.type = "text";
inputData.placeholder = "Поле вводу даних";
spanOutput.className = "spanOutput";

document.body.append(inputData);
inputData.after(spanOutput);

inputData.addEventListener("input", (e) => {
    spanOutput.innerText = `Дані: ${e.target.value}`
})
/*9 При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. 
Це поле буде служити для введення числових значень - Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. 
При втраті фокусу вона пропадає. - Коли вибрано фокус з поля - його значення зчитується, 
над полем створюється `span`, в якому має бути виведений текст: `Поточна ціна: 
${значення з поля введення}`.  Поруч із ним має бути кнопка з хрестиком (`X`). 
Значення всередині поля введення фарбується  зеленим. - При натисканні на `Х` 
- `span` з текстом та кнопка `X` повинні бути видалені. - Якщо користувач ввів 
число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при 
цьому не створюється.*/

    const input = document.createElement("input"),
        span = document.createElement("span"),
        x = document.createElement("button");

    input.placeholder = "Price";
    input.className = "num";
    input.type = "number";
    x.innerText = "X";

    //input.setAttribute("type", "number");
    input.addEventListener("focus", (e) => {
        input.classList.add("border-focus");
        input.before(span);
        span.after(x);
    });

    x.addEventListener("click", () => {
        x.remove();
        span.remove();
        input.value = ""
    });

    input.addEventListener("input", (e) => {
        span.innerText = `Поточна ціна: ${e.target.value}`
    });

    input.addEventListener("blur", () => {
        input.classList.remove("border-focus");
        if (input.value < 0) {
            input.classList.add("error");
            input.after("Please enter correct price")
        }
    })
    document.body.prepend(input);
})