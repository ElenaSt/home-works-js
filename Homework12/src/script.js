/*У файлі  лежить розмітка для кнопок.
Кожна кнопка містить назву клавіш - та кнопка, 
на якій написана ця літера, повинна фарбуватись у синій колір. При 
цьому якщо якась інша літера вже раніше була пофарбована в синій колір
- вона стає чорною. Наприклад за натисканням Enter перша кнопка
забарвлюється в синій колір. Далі, користувач натискає "S", і
кнопка "S" забарвлюється в синій колір, а кнопка Enter знову стає чорною.
S-83 E-83 O-79 N-78 L-76 Z-90 Enter-13*/
document.addEventListener("DOMContentLoaded", function(){
    
    let [...arr] = document.querySelectorAll(".btn");
    let main = document.getElementById("main");
    //for (let {textContent} of document.querySelectorAll(".btn")) arr.push(textContent);
    //console.log(arr);
    
    main.addEventListener ("keydown", (event) => {
        let keyName = event.key;
        for (let i=0; i<arr.length; i++) {
            if (keyName  == arr[i].id) {
                arr[i].style.color = "blue";
            } else if (keyName !== arr[i].id && arr[i].style.color == "blue") {
                arr[i].style.color = "grey";
            }
        }
    });
});


