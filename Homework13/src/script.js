let url = "https://swapi.dev/api/people";
//створюємо запит на сервер за допомогою fetch
const data = fetch(url, { method: "get" });
//робота з запитом, обробка як Promise
let data1 = data.then((rez) => rez.json(), (error) => console.error(error));

/*data1.then((rez1) => {
    showList(rez1.results);
});*/
let arr = [];
//Масив на localStorage
localStorage.men = JSON.stringify([]);

class Man {
    constructor(name, gender, height, hair_color, skin_color) {
        this.name = name;
        this.gender = gender;
        this.height = height;
        this.hair_color = hair_color;
        this.skin_color = skin_color;
    }
    showList() {
    
        const cardBody = document.createElement("div");
        const cardTitle = document.createElement("h5");
        const order = document.createElement("div");
        const cardText = document.createElement("div");
        const heightLi = document.createElement("div");
        const massLi = document.createElement("div");
        const hairColorLi = document.createElement("div");
        const skinColorLi = document.createElement("div");
        const button = document.createElement("input");

        cardTitle.innerText = "Name: " + this.name;
        heightLi.innerText = "gender: " + this.gender;
        massLi.innerText = "height: " + this.height;
        hairColorLi.innerText = "hair_color: " + this.hair_color;
        skinColorLi.innerText = "skin_color: " + this.skin_color;

        document.querySelector(".card_man").append(cardBody);
        cardBody.append(cardTitle, order);
        order.append(cardText);
        cardText.append(heightLi, massLi, hairColorLi, skinColorLi);
        cardBody.after(button);

        cardBody.classList = "cardBody";
        cardTitle.classList = "cardTitle";
        order.classList = "order";
        cardText.classList = "cardText";
        button.id = this.name;
        button.type = "button";
        button.value = "Save";
        button.classList = "btn";

        
}
};
//let processing = data1.then((element) => element.json());

data1.then((rez1) => {
    rez1.results.forEach(element => {
        let user = new Man(
            element.name,
            element.gender,
            element.height,
            element.hair_color,
            element.skin_color);

        user.showList();
        arr.push(user);
    });

});

//Зберігаємо в localStorage.people
document.querySelector(".card_man").onclick = (e) => {
    let input = e.target;
    if (input.tagName != "INPUT") return;
    let data = input.id;
    console.log(data);
    arr.forEach(el => {
        if (el.name === data) {
            let temp = JSON.parse(localStorage.men);
            temp.push(el);
            console.log(temp);
            localStorage.setItem("people", JSON.stringify(temp));
        }
    });
};


   
/*
let url = "https://swapi.dev/api/people";
//створюємо запит на сервер за дпомогою fetch
const data = fetch(url, { method: "get" });
робота з запитом, обробка як Promise
let data1 = data.then((rez) => rez.json(), (error) => console.error(error));

data1.then((rez1) => {
    showList(rez1.results);
});

Масив на localStorage
localStorage.people = JSON.stringify([]);


function showList(arr) {
    arr.forEach(element => {
        const { name, height, mass, hair_color, skin_color } = element;

        const cardBody = document.createElement("div");
        const cardTitle = document.createElement("h5");
        const order = document.createElement("div");
        const cardText = document.createElement("div");
        const heightLi = document.createElement("div");
        const massLi = document.createElement("div");
        const hairColorLi = document.createElement("div");
        const skinColorLi = document.createElement("div");
        const button = document.createElement("input");

        cardTitle.innerText = "Name: " + name;
        heightLi.innerText = "height: " + height;
        massLi.innerText = "mass: " + mass;
        hairColorLi.innerText = "Color of hair: " + hair_color;
        skinColorLi.innerText = "Color of skin: " + skin_color;

        document.body.append(cardBody);
        cardBody.append(cardTitle, order);
        order.append(cardText);
        cardText.append(heightLi, massLi, hairColorLi, skinColorLi);
        cardBody.after(button);

        cardBody.classList = "cardBody";
        cardTitle.classList = "cardTitle";
        order.classList = "order";
        cardText.classList = "cardText";
        button.id = name;
        button.type = "button";
        button.value = "Save";
        button.classList = "btn";

        //Зберігаємо в localStorage.people
        let saveCart = (e) => {
            let input = e.target;
            if (input.tagName != "INPUT") return;
            let data = input.id;
            console.log(data);
            arr.forEach(el => {
                if (el.name === data) {
                    let temp = JSON.parse(localStorage.people);
                    temp.push(el);
                    console.log(temp);
                    localStorage.setItem("people", JSON.stringify(temp));
                }
            });
        };
        button.addEventListener("click", saveCart);
    })
   
};
css:
body {
    display: flex;
    background-color: #b6e4ec;
    flex-direction: row;
    align-items: flex-start;
}

*/

