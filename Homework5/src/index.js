/*Створити об'ект Документ, в якому зробити властивості - Заголовок, тіло, кінець, дата. 
Створити в об'екті вкладений  об'ект - Додаток. 
Створити в об'екті Додаток, вкладені об'екти: Заголовок, тіло, кінець, дата. 
Створити метод для заповнювання документу.
Створити метод для заповнювання вкладеного Додатку
Створити метод відображення документу.
Cтворити метод відображення додатку документу.  */

let newDocument = {
    title: "",
    body: "",
    footer: "",
    date: "",
    appendix: {
        title: {text: ""},
        body: {text: ""},
        footer: {text: ""},
        date: {text: ""},
        },
    

    inputDoc: function () {
        this.title = prompt ("Введіть заголовок", "Заголовок");
        this.body = prompt ("Введіть інформацію про об'єкт", "Інформація про об'єкт") ;
        this.footer = prompt ("Введіть контакти", "Контакти");
        this.date = prompt ("Введіть дату", new Date());
    },

    inputAppendix: function () {
    this.appendix.title.text = prompt ("Введіть заголовок додатку", "Заголовок додатку");
    this.appendix.body.text = prompt ("Введіть інформацію про додаток об'єкту", "Інформація про додаток") ;
    this.appendix.footer.text = prompt ("Введіть адресу", "Адреса");
    this.appendix.date.text = prompt ("Введіть дату", "11.06.2022");
    },

    outputDoc: function () {
        document.write(`Заголовок документу - "${this.title}"`);
        document.write(`<br> Тіло документа - "${this.body}"`);
        document.write(`<br> Кінець документа - "${this.footer}"`);
        document.write(`<br> Дата документа - "${this.date}"`);
    },

    outputAppendix: function () {
      document.write("<br>");
      document.write(`<br> Назва програми - "${this.appendix.title.text}"`);
      document.write(`<br> Тіло програми - "${this.appendix.body.text}"`);
      document.write(`<br> Кінець документа - "${this.appendix.footer.text}"`);
      document.write(`<br> Дата програми - "${this.appendix.date.text}"`);
    },
} 
  
newDocument.inputDoc ();
newDocument.inputAppendix ();
  
newDocument.outputDoc ();
newDocument.outputAppendix ();
