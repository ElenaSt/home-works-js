
        //1. Розробити ф-цію-кон-р, яка буде створювати об'єкт Human.
            function Human(name, age) {
                this.name = name;
                this.age = age;
            }
            let humans = [];
            humans.push( new Human ("Alex", 23));
            humans.push( new Human ("Maria", 32));
            humans.push( new Human ("Maya", 18));
            humans.push( new Human ("Den", 24));

            console.log (humans);     
        //Створити функцію сортування значення аge у порядку зростання :
           function humanSortByAge1(array) {
                let arrayByhuman = [], c = 0;
                for (let i = 0; i < array.length; i++) {
                    for (let j = 0; j < array.length; j++) {
                        if (array[i].age > array[j].age) {
                            c++;
                    }
                }
                arrayByhuman[c] = array[i];
                c = 0;
            }
            return arrayByhuman;
        }
        console.log (humanSortByAge1(humans));
        //або let humansByAgeUp = humans.sort((prev,next) =>  prev.age - next.age);
        //  console.log(humansByAgeUp);
        
        //Створити функцію сортування значення аge у порядку спадання:
        function humanSortByAge2(array) {
                let arrayByhuman = [], c = 0;
                for (let i = 0; i < array.length; i++) {
                    for (let j = 0; j < array.length; j++) {
                        if (array[i].age < array[j].age) {
                            c++;
                    }
                }
                arrayByhuman[c] = array[i];
                c = 0;
            }
            return arrayByhuman;
        }
        console.log (humanSortByAge2(humans));
       //2. Створити функцію конструктор. Наслідування, методи.
        //Функція конструктор
        function Humannew (name, surname, age) {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.sayHi = function () {
            return ("Привіт, я - " + name + " " + surname + ". Мені " + age + ". ");
            } ;
        }
        //створення екземпляра   
        var student = new Humannew("Олексій", "Красовський",21);
        
        console.log(student.sayHi());

        //наслідування ознак прототипу та додавання ознак екземпляру
        let worker = new Humannew("Єгор", "Красовський", 28);
        worker.speciality = "Front-End Developer";
        worker.sayHiNew = function () {
        return (this.sayHi() + "Моя спеціалізація:" + this.speciality);
        };

        console.log(worker.sayHiNew());

        