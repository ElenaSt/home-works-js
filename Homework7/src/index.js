/*Реалізувати функцію створення об'єкта "користувач".
Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
При виклику функція повинна запитати у людини, що викликає, ім'я та прізвище.
Використовуючи дані, введені користувачем, створити об'єкт newUser з властивостями firstName та lastName. 
Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені користувача,
з'єднане з прізвищем користувача, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
Створити користувача за допомогою функції createNewUser(). Викликати користувача функцію getLogin().
Вивести у консоль результат виконання функції.
*/
/* Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Візьміть виконане
 завдання вище (створена вами функція createNewUser()) та доповніть її наступним  функціоналом: При виклику 
 функція повинна запитати у дату народження, що викликає, (текст у форматі dd.mm.yyyy)   і зберегти її 
 в полі birthday. Створити метод getAge() який повертатиме скільки користувачеві років. Створити метод 
 getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, поєднану з прізвищем 
 (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ікравченко1992). 
 Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge()та getPassword() 
 створеного об'єкта. */


class CreateNewUser {
    constructor(firstName, lastName, birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday

    }

    getAge() {
      return   new Date().getFullYear() - this.birthday.split(".")[2] 
    }

    getPassword() {
        return `${this.firstName[0].toLocaleUpperCase()}${this.lastName.toLocaleLowerCase()}${this.birthday.split(".")[2]}`
    }

    getLogin(){
        return `${this.firstName[0].toLocaleLowerCase()}${this.lastName.toLocaleLowerCase()}` 
    }
}

const newUser = new CreateNewUser(prompt("name"), prompt("lastName"), prompt("birthday", "dd.mm.yyyy"))

console.log("Логін: " + newUser.getLogin());
console.log("Вік: " + newUser.getAge());
console.log("Пароль: " + newUser.getPassword());

/*Реалізувати функцію фільтру масиву за укзаним типом даних. Написати функцію filterBy(), котра буде приймати
в себе 2 аргументи. Перший аргумент - масив, котрий буде вміщати в собі будь-які дані, другий аргумент -
тип даних. Функція повинна повернути новий масив, котрий буде вміщати в собі всі дані, котрі були передані 
в аргумент, за виключенням тих, тип яких був переданий другим аргументом. Тобто, якщо передати 
масив ["hello", "word", 23, "23", null], та другим елементом передати 'string', тоді офункція поверне масив 
[23, null].*/

const arr = ['html', 'js', 15, 'css', 25, null];
function fun (anyarr, type) {
   return anyarr.filter ((element) => type !== typeof element)
}
const another_arr = fun (arr, 'string');
console.log (another_arr);

