//Завдання 1.Таймер
let counter = 0, intervalHandler;
        const get = id => document.getElementById(id);
        let ms = get('ms');
        let second = get('second');
        let minute = get('minute');
        
        const count = () => {
            get("output").innerText = counter;
            counter++;
        }
        get("startButton").onclick = () => {
            get("output").style.backgroundColor = "green";            
            intervalHandler = setInterval(function() {
            counter += 1/60;
            msVal = Math.floor((counter - Math.floor(counter))*100);
            secondVal = Math.floor(counter) - Math.floor(counter/60) * 60;
            minuteVal = Math.floor(counter/60);
            ms.innerHTML = msVal < 10 ? "0" + msVal.toString() : msVal;
            second.innerHTML = secondVal < 10 ? "0" + secondVal.toString() : secondVal;
            minute.innerHTML = minuteVal < 10 ? "0" + minuteVal.toString() : minuteVal;
            }, 1000/60);
        }
        get("stopButton").onclick = () => {
            clearInterval(intervalHandler); // зупиняємо таймер, по якому викликається функція count
            get("output").style.backgroundColor = "red";
        }
        get("resetButton").onclick = () => {
            clearInterval(intervalHandler); // сброс
            get("output").style.backgroundColor = "grey";
            msVal=secondVal=0;
            secondVal=0;
            minuteVal=0;
            ms.innerText = `0${msVal}`;
            second.innerText = `0${secondVal}`;
            minute.innerText = `0${minuteVal}`;
        }
        //Завдання 2.Програма перевірки телефону.
        //поле для введення телефону та кнопка збереження
        let input = document.createElement("input");
        input.placeholder = "000-000-00-00"
        input.id = "phoneInput";
        document.body.append(input);

        let button = document.createElement("input");
        button.classList.add("button");
        button.type = "button";
        button.value = "Зберегти";
        document.body.append(button);

        let div = document.createElement("div");
        input.before(div);
        let pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;

        button.onclick = () => {
        let number = input.value;
        if (pattern.test(number)) {
            input.style.backgroundColor = "green";
            document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg';
        } else {
            div.innerText = "Невірно.Формат телефону повинен бути: 000-000-00-00";
        }
        }
        

        