//Завдання 3. Слайдер, який кожні 3 сек змінюватиме зображення 
let pictures = [
    "https://sparkmedia.com.ua/wp-content/uploads/2020/01/beautiful-countries-1024x682.jpg",
    "https://sparkmedia.com.ua/wp-content/uploads/2020/01/uar-1024x676.jpg",
    "https://sparkmedia.com.ua/wp-content/uploads/2020/01/china.jpg",
    "https://sparkmedia.com.ua/wp-content/uploads/2020/01/italy-1024x576.jpg",
    "https://sparkmedia.com.ua/wp-content/uploads/2020/01/ireland-1024x576.jpg"
    ];

    let img = document.querySelector("img");
    let slider = 0;
    const sliderImg = () => {
    img.setAttribute("src", pictures[slider]);
    slider++;
    if (slider === pictures.length) {
        slider = 0;
    }
    setTimeout(sliderImg, 3000);
    }
    sliderImg();