import React from "react";

function Table(props) {
    return (<table>
         <tbody>
            <tr>
                <th>№ валюти</th>
                <th>Назва валюти</th>
                <th>Ціна</th>
            </tr>
        {props.data.map((element, index) => {
          return <tr key={index}>
            <td>{element.r030}</td>
            <td>{element.txt}</td>
            <td>{element.rate}</td>
            </tr>;
        })}
      </tbody>
    </table>
    );
}

export default Table